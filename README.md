# Test build mapbox-gl library
## Clone
```bash
git clone https://gitlab.com/antoinealej/test-lib-mapbox-gl-build.git
```
## Setup
Add your mapbox-gl access token in `app/.env.defaults` or 
```bash
cp app/.env.defaults app/.env.local
```
then add your mapbox-gl access token in `app/.env.local`
## Start
```bash
npm i
npm run build
npm start
```
## Access
http://localhost:8080/
## Result
![result](docs/result.png "Result")
## Error
```
Uncaught ReferenceError: _createClass is not defined
```
![error](docs/error.png "Error")
